/*
    CryptIt
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include <QShortcut>
#include <QFileDialog>
#include <QCryptographicHash>

#include "aes.h"

#include "cryptit.h"
#include "ui_cryptit.h"

// Demo IV
// Not Used here
/*
const uint8_t iv[] = { 0xf0, 0xe1, 0xd2, 0xc3, 0xb4, 0xa5, 0x96,
		  0x87, 0x78, 0x69, 0x5a, 0x4b, 0x3c, 0x2d, 0x5e, 0xaf };
*/


CryptIt::CryptIt(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::CryptIt)
{
	ui->setupUi(this);

	startSetup();
}

CryptIt::~CryptIt()
{
	delete ui;
}

void CryptIt::on_browse_clicked()
{
	QString filepath = QFileDialog::getOpenFileName(this, "Select file to encrypt/decrypt", QDir::homePath());

	if (!filepath.count()) {
		qDebug() << "ERROR:" << "No file selected.";
		return;
	}

	ui->filepath->setText(filepath);

	if (filepath.count()) {
		ui->next->setEnabled(true);
	} else {
		ui->next->setEnabled(false);
	}
}

void CryptIt::on_keyPlain_textChanged(const QString &arg1)
{
	ui->next->setEnabled(arg1.count());
}

void CryptIt::on_ivPlain_textChanged(const QString &arg1)
{
	ui->next->setEnabled(arg1.count());
}

void CryptIt::on_next_clicked()
{
	if (!ui->next->isEnabled()) {
		return;
	}

	ui->prev->setEnabled(true);
	ui->next->setEnabled(false);

	if ((ui->stackedWidget->currentIndex() == 0 && ui->ivPlain->text().count()) ||
			(ui->stackedWidget->currentIndex() == 1 && ui->filepath->text().count())) {
		ui->next->setEnabled(true);
	}

	ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex() + 1);

	if (ui->stackedWidget->currentIndex() == 3) {
		ui->next->setEnabled(true);
	}

	if (ui->stackedWidget->currentIndex() == 4) {
		if (ui->actionGroup->checkedId()) { // Decrypt
			ui->plainTextEdit->setPlainText(
						decryptText(
							fileText(ui->filepath->text()).toHex(),
							ui->keyPlain->text(),
							ui->ivPlain->text()
						));
		} else { // Encrypt
			ui->plainTextEdit->setPlainText(
						encryptText(
							fileText(ui->filepath->text()),
							ui->keyPlain->text(),
							ui->ivPlain->text()
						));
		}
	}
}

void CryptIt::on_prev_clicked()
{
	if (!ui->prev->isEnabled()) {
		return;
	}

	ui->next->setEnabled(true);

	if (ui->stackedWidget->currentIndex() == 1) {
		ui->prev->setEnabled(false);
	}

	ui->stackedWidget->setCurrentIndex(ui->stackedWidget->currentIndex() - 1);
}

void CryptIt::on_showPass1_clicked(bool checked)
{
	togglePass(checked, ui->keyPlain, ui->showPass1);
}

void CryptIt::on_showPass2_clicked(bool checked)
{
	togglePass(checked, ui->ivPlain, ui->showPass2);
}

void CryptIt::on_saveAsEnc_clicked()
{
	if (ui->actionGroup->checkedId() == 0) { // encrypt
		QString filepath = QFileDialog::getSaveFileName(this, "Save your encryted text in", QDir(ui->filepath->text()).path());
		if (filepath.count()) {
			QFile file(filepath);
			if (!file.open(QIODevice::ReadWrite | QIODevice::Truncate)) {
				qDebug() << "Internal file not open error";
				return;
			}

			file.write(QByteArray::fromHex(ui->plainTextEdit->toPlainText().toLatin1()));
			file.close();
		} else {
			qDebug() << "Nothing selected to save the encryted text";
		}
	}
}

void CryptIt::on_saveAsDec_clicked()
{
	if (ui->actionGroup->checkedId() == 1) { // decrypt
		QString filepath = QFileDialog::getSaveFileName(this, "Save your decryted text in", QDir(ui->filepath->text()).path());
		if (filepath.count()) {
			QFile file(filepath);
			if (!file.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
				qDebug() << "Internal file not open error";
				return;
			}
			QTextStream out(&file);
			out << ui->plainTextEdit->toPlainText();
			file.close();
		} else {
			qDebug() << "Nothing selected to save the decryted text";
		}
	}
}

void CryptIt::startSetup()
{
	ui->stackedWidget->setCurrentIndex(0);
	ui->prev->setEnabled(false);
	ui->next->setEnabled(false);
	ui->actionGroup->setId(ui->encrypt, 0);
	ui->actionGroup->setId(ui->decrypt, 1);

	connect(ui->keyPlain, &QLineEdit::returnPressed, this, &CryptIt::on_next_clicked);
	connect(ui->ivPlain, &QLineEdit::returnPressed, this, &CryptIt::on_next_clicked);
	connect(ui->filepath, &QLineEdit::returnPressed, this, &CryptIt::on_next_clicked);

	QShortcut *shc;
	shc = new QShortcut(QKeySequence(Qt::Key_Left), this);
	connect(shc, &QShortcut::activated, this, &CryptIt::on_prev_clicked);
	shc = new QShortcut(QKeySequence(Qt::Key_Right), this);
	connect(shc, &QShortcut::activated, this, &CryptIt::on_next_clicked);
}

void CryptIt::togglePass(bool checked, QLineEdit *pass, QToolButton *s)
{
	QString icon = "password-show-on";
	QLineEdit::EchoMode em = QLineEdit::Normal;

	if (!checked) {
		icon = "password-show-off";
		em = QLineEdit::Password;
	}

	s->setIcon(QIcon::fromTheme(icon));
	pass->setEchoMode(em);
}

bool CryptIt::checkIsEmpty()
{
	if (!ui->filepath->text().count()) {
		qDebug() << "ERROR:" << "Empty file path.";
		return true;
	}

	if (!ui->keyPlain->text().count()) {
		qDebug() << "ERROR:" << "No key entered";
		return true;
	}

	if (!ui->ivPlain->text().count()) {
		qDebug() << "ERROR:" << "No iv entered.";
		return true;
	}

	QFile file(ui->filepath->text());

	if (!file.exists()) {
		qDebug() << "ERROR:" << "File not exist.";
		return true;
	}

	return false;
}

QByteArray CryptIt::fileText(QString filepath) const
{
	QFile file(filepath);
	QByteArray data = nullptr;
	if (file.open(QIODevice::ReadOnly)) {
		data = file.readAll();
		file.close();
	} else {
		qDebug() << "File open error";
	}
	return data;
}

QByteArray CryptIt::keyHash(QString keyPlain) const
{
	QCryptographicHash hash(QCryptographicHash::Sha256);
	hash.addData(keyPlain.toUtf8());
	QByteArray res = hash.result();
	return res;
}

int CryptIt::getAlignedSize(int currSize, int alignment)
{
	int padding = (alignment - currSize % alignment) % alignment;
	return currSize + padding;
}

QString CryptIt::encryptText(const QString &rawText, const QString &key, const QString &ivPlain) {
	const uint8_t *iv = (const uint8_t *)ivPlain.toLatin1().data();
	QByteArray keyData = keyHash(key);
	const ushort *rawData = rawText.utf16();
	void *rawDataVoid = (void*)rawData;
	const char *rawDataChar = static_cast<const char*>(rawDataVoid);
	QByteArray inputData;
	inputData.append(rawDataChar, rawText.size() * sizeof(QChar) + 1);

	const int length = inputData.size();
	int encryptionLength = getAlignedSize(length, 16);
	Q_ASSERT(encryptionLength % 16 == 0 && encryptionLength >= length);

	inputData.resize(encryptionLength);
	for (int i = length; i < encryptionLength; i++) { inputData[i] = 0; }

	struct AES_ctx ctx;
	AES_init_ctx_iv(&ctx, (const uint8_t*)keyData.data(), iv);
	AES_CBC_encrypt_buffer(&ctx, (uint8_t*)inputData.data(), encryptionLength);

	QString hex = QString::fromLatin1(inputData.toHex());
	return hex;
}

QString CryptIt::decryptText(const QString &hexEncodedText, const QString &key, const QString &ivPlain) {
	const uint8_t *iv = (const uint8_t *)ivPlain.toLatin1().data();
	QByteArray keyData = keyHash(key);
	const int length = hexEncodedText.size();
	int encryptionLength = getAlignedSize(length, 16);

	QByteArray encodedText = QByteArray::fromHex(hexEncodedText.toLatin1());
	const int encodedOriginalSize = encodedText.size();
	Q_ASSERT(encodedText.length() <= encryptionLength);
	encodedText.resize(encryptionLength);
	for (int i = encodedOriginalSize; i < encryptionLength; i++) { encodedText[i] = 0; }

	struct AES_ctx ctx;
	AES_init_ctx_iv(&ctx, (const uint8_t*)keyData.data(), iv);
	AES_CBC_decrypt_buffer(&ctx, (uint8_t*)encodedText.data(), encryptionLength);

	encodedText.append("\0\0");
	void *data = encodedText.data();
	const ushort *decodedData = static_cast<ushort *>(data);
	QString result = QString::fromUtf16(decodedData, -1);
	return result;
}
