/*
    CryptIt
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CRYPTIT_H
#define CRYPTIT_H

#include <QWidget>

class QLineEdit;
class QToolButton;

namespace Ui {
class CryptIt;
}

class CryptIt : public QWidget
{
    Q_OBJECT

public:
    explicit CryptIt(QWidget *parent = nullptr);
    ~CryptIt();

private slots:
	void on_browse_clicked();
    void on_keyPlain_textChanged(const QString &arg1);
    void on_ivPlain_textChanged(const QString &arg1);
	void on_next_clicked();
	void on_prev_clicked();
	void on_showPass1_clicked(bool checked);
	void on_showPass2_clicked(bool checked);
	void on_saveAsEnc_clicked();
	void on_saveAsDec_clicked();

private:
	Ui::CryptIt *ui;

	void startSetup();
	void togglePass(bool checked, QLineEdit *pass, QToolButton *s);

	bool checkIsEmpty();
	QByteArray fileText(QString filepath) const;
	QByteArray keyHash(QString keyPlain) const;
	int getAlignedSize(int currSize, int alignment);

	QString encryptText(const QString &rawText, const QString &key, const QString &ivPlain);
	QString decryptText(const QString &hexEncodedText, const QString &key, const QString &ivPlain);
};

#endif // CRYPTIT_H
