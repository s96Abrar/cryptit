QT       += core gui widgets

TEMPLATE = app
TARGET = cryptit

CONFIG += c++11

LIBS += -lcryptopp

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }

        target.path	  = $$PREFIX/bin/
        INSTALLS	 += target
}

SOURCES += \
    aes.cpp \
    cryptit.cpp \
    main.cpp

HEADERS += \
    aes.hpp \
    aes.h \
    cryptit.h

FORMS += \
    cryptit.ui
