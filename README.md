# CryptIt

An AES Encryption of 128, 196 and 256 bit using [Tiny-AES-C](https://github.com/kokke/tiny-AES-c). Using a sample code from [here](https://intmaker.com/2015/text-encryption-in-qtc-with-tiny-aes-128bit/).